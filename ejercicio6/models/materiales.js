class Materiales {
  constructor() {
    this._listado = [];
  }


  crearMaterial(material = {}) {
    this._listado[material.id] = material;
  }


  get listArray() {
    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const material = this._listado[key];
      listado.push(material);
    })
    return listado;
  }


  cargarMaterialesFromArray(personas = []) {
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }


  eliminarMaterial(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

}

module.exports = Materiales