// uuid 
const { v4: uuidv4 } = require('uuid');

class Material {
  id = '';
  material = []; 
  precio_unitario = [];
  precio_total = 0;

  constructor(material, precio_unitario, precio_total) {
    this.id = uuidv4();
    this.material = material;
    this.precio_unitario = precio_unitario;
    this.precio_total = precio_total;
  }

  getId(idx) {
    this.id = idx
  }
}


module.exports = Material;