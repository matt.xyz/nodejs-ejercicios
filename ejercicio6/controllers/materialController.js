const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Material = require('../models/material');
const Materiales= require('../models/materiales');
const { response } = 'express';


const materiales = new Materiales();

let materialDB = leerDB();
if (materialDB) {
  materiales.cargarMaterialesFromArray(materialDB);
}

const materialGet = (req, res = response) => {

  res.json({
    message: 'get API, listo',
    materialDB
  })
}


const materialPut = (req, res = response) => {
  const { id } = req.params;

  if (id) {
    materiales.eliminarMaterial(id);
    const { material, precio_unitario, precio_total } = req.body;
    const unMaterial = new Material(material, precio_unitario, precio_total);

    unMaterial.getId(id);
    materiales.crearMaterial(unMaterial);
    guardarDB(materiales.listArray);

    materialDB = leerDB();
  }

  res.json({
    message: 'put API, listo',
    materialDB
  })
}


const materialPost = (req, res = response) => {

  const { material, precio_unitario, precio_total } = req.body;
  const unMaterial = new Material(material, precio_unitario, precio_total);

  materiales.crearMaterial(unMaterial);
  guardarDB(materiales.listArray);

  const listado = leerDB();
  materiales.cargarMaterialesFromArray();

  res.json({
    message: 'post API, listo',
    listado
  })
}


const materialDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    materiales.eliminarMaterial(id);
    guardarDB(materiales.listArray)
  }

  res.json({
    message: 'Se elimino el articulo de su carrito de compras',
  })
}


module.exports = {
  materialGet,
  materialPut,
  materialPost,
  materialDelete
}