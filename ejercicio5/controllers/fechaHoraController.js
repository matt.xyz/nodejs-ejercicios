const { request, express } = 'express';

const fechaHoraGet = (req = request, res = response) => {

  let fecha = new Date();
  let diaSemana = fecha.getDay();
  let mesAnio = fecha.getMonth();
  let year = fecha.getFullYear();
  let date_2 = fecha.toLocaleDateString();
  let horas = fecha.getHours();
  let minutos = fecha.getMinutes();
  let segundos = fecha.getSeconds();
  let day;
  let date_1;
  let hour = horas + ':' + minutos + ':' + segundos;
  let prefix;

  let semana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  day = semana[diaSemana]

  let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  date_1 = diaSemana + ' de ' + meses[mesAnio];

  if (horas >= 12) {
    horas = horas - 12;
    prefix = 'PM';
  
  } else {
    prefix = 'AM';
  }

  res.json({
    message: "Esta es la fecha y hora del Servidor",
    day,
    date_1,
    year,
    date_2,
    hour,
    prefix
  })
}


module.exports = fechaHoraGet;