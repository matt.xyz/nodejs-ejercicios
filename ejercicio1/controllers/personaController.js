const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');
const { response } = 'express';


const personas = new Personas();

let personaDB = leerDB();
if (personaDB) {
  personas.cargarPersonasFromArray(personaDB);
}

const personaGet = (req, res = response) => {

  res.json({
    message: 'get API, listo',
    personaDB
  })
}

const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    
    personas.eliminarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

    persona.getId(id)

    personas.crearPersona(persona);
   
    guardarDB(personas.listArray);

    personaDB = leerDB()
  }


  res.json({
    message: 'put API, listo',
    personaDB
  })
}

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);

  personas.crearPersona(persona);
  guardarDB(personas.listArray);

  const listado = leerDB();
  personas.cargarPersonasFromArray(listado)

  res.json({
    message: 'post API, listo',
    listado
  })
}

const personaDelete = (req, res = response) => {
  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    guardarDB(personas.listArray)

    personaDB = leerDB()
  }

  res.json({
    message: 'delete API, listo',
    personaDB
  })
}


module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}