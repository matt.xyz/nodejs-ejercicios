const Persona = require("./persona");

class Personas {
  constructor() {
    this._listado = [];
  }

  crearPersona(persona = {}) {
    this._listado[persona.id] = persona;
  }

  get listArray() {
    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const persona = this._listado[key];
      listado.push(persona);
    })
    return listado;
  }

  cargarPersonasFromArray(personas = []) {
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }

  eliminarPersona(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

}

module.exports = Personas